const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name : {
        type : String,
        require : [ true, "Product name is required" ]
    },
    description : {
        type : String,
        require : [ true, "Product description is required"]
    },
    price : {
        type : Number,
        require : [ true, "Product price is required" ]
    },
    imgLink : {
        type : String
    },
    isActive : {
        type : Boolean,
        default : true
    },
    createdOn : {
        type : Date,
        default : new Date()
    },
    userOrders : [
        {
            userId : {
                type : String,
                require : [ true, "UserId is required" ]
            }
        }
    ]
});

module.exports = mongoose.model("Product", productSchema);