const bcrypt = require("bcrypt");

const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");

// USER REGISTRATION CONTROLLER
module.exports.registerUser = reqBody => {
    return User.find({ email : reqBody.email }).then(result => {
        if(result.length > 0){
            return false;
        }else{
            let newUser = new User({
                firstName : reqBody.firstName,
                lastName : reqBody.lastName,
                email : reqBody.email,
                password : bcrypt.hashSync(reqBody.password, 10)
            });
            return newUser.save().then(user => true).catch(err => {
                console.log(err);
                return false;
            });
        };
    }).catch(err => {
        console.log(err);
        console.log(`error on DB.find`);
        return false;
    });
};

// USER LOGIN CONTROLLER
module.exports.loginUser = reqBody => {
    return User.findOne({ email : reqBody.email }).then(result => {
        if(result == null){
            return false;
        }else{
            const passwordMatched = bcrypt.compareSync(reqBody.password, result.password);
            if(passwordMatched){
                return { access : auth.createAccessToken(result) };
            }else{
                return false;
            };
        };
    });
};


// GET USER DETAILS CONTROLLER
module.exports.getUserDetails = userId => {
    return User.findById(userId).then(result => {
        result.password = "";
        return result;
    }).catch(err => {
        console.log(err);
        return false;
    });
};


// CHECKOUT ORDERS CONTROLLER
module.exports.checkOut = async (data) => {
    console.log(data);

    let product = await Product.findById(data.productId).then(product => product);
    console.log(product);

    let isUserUpdated = await User.findById(data.userId).then(user => {
        let productCheckedOut = {
            products : [
                {
                    productId : data.productId,
                    productName : product.name,
                    quantity : data.quantity
                }
            ],
            totalAmount : (product.price * data.quantity),
        };

        
        user.orderedProduct.push(productCheckedOut);

        return user.save().then(user => true).catch(err => {
            console.log(err);
            return false;
        });
    }).catch(err => {
        console.log(err);
        console.log(`something went wrong while updating user`);
        return false;
    });


    let isProductUpdated = await Product.findById(data.productId).then(product => {
        product.userOrders.push({ userId : data.userId });

        return product.save().then(product => true).catch(err => {
            console.log(err);
            return false;
        });
    }).catch(err => {
        console.log(err);
        console.log(`something went wrong while updating product`);
        return false;
    });

    return (isUserUpdated && isProductUpdated) ? true : false;
};






/* STRETCH GOALS STARTS HERE */


// SET USER AS ADMIN !ADMIN ONLY!
module.exports.setUserAdmin = (reqParams) => {
    let updateAdminField ={
        isAdmin : true
    };

    return User.findOneAndUpdate({ email : reqParams.email}, updateAdminField).then(user => true).catch(err => {
        console.log(err);
        return false;
    });
};

// GET ALL USER'S ORDERS
module.exports.getUserOrders = (userId) => {
    return User.findById(userId).then(result => {
        if(result){
            let orders = result.orderedProduct;
            return orders;
        }else{
            return false;
        };
    }).catch(err => {
        console.log(err);
        console.log(`something went wrong in finding user`)
        return false
    });
};



// GETTING ALL ORDERED PRODUCTS !ADMIN ONLY!
module.exports.getAllOrdered = () => {
    return User.find({}, "orderedProduct").then(ordered => {
        let orders = ordered.filter(item => item.orderedProduct.length > 0).map(item => item.orderedProduct);
        console.log(orders);
        return orders;
    }).catch(err => {
        console.log(err);
        console.log(`something went wrong in mapping orders`);
        return false;
    });
};