const Product = require("../models/Product");


// ADDING NEW PRODUCT CONTROLLER !ADMIN ONLY!
module.exports.addNewProduct = reqBody => {
    let newProduct = new Product({
        name : reqBody.name,
        description : reqBody.description,
        price : reqBody.price,
        imgLink : reqBody.imgLink
    });

    return newProduct.save().then(product => true).catch(error => {
        console.log(err);
        return false;
    });
};

// GETTING ALL PRODUCTS CONTROLLER
module.exports.getAllProducts = () => {
    return Product.find({}).then(result => result).catch(err => {
        console.log(err);
        return false;
    });
};

// GETTING ALL ACTIVE ONLY PRODUCTS CONTROLLER
module.exports.getAllActiveProducts = () => {
    return Product.find({ isActive : true }).then(result => result).catch(err => {
        console.log(err);
        return false;
    });
};

// GETTING SPECIFIC PRODUCT CONTROLLER
module.exports.getProduct = reqParams => {
    return Product.findById(reqParams.productId).then(result => result).catch(err => {
        console.log(err);
        return false;
    });
};

// UPDATING PRODUCT CONTROLLER
module.exports.updateProduct = (reqParams, reqBody) => {
    console.log(reqBody);
    let updatedProduct = {
        name : reqBody.name,
        description : reqBody.description,
        price : reqBody.price,
        imgLink : reqBody.imgLink
    };

    return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then(product => true).catch(err => {
        console.log(err);
        return false;
    });
};

// ARCHIVING PRODUCT CONTROLLER
module.exports.archiveProduct = (reqParams) => {
    let updateActiveField ={
        isActive : false
    };

    return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then(product => true).catch(err => {
        console.log(err);
        return false;
    });
};

// ACTIVATING PRODUCT CONTROLLER
module.exports.activateProduct = (reqParams) => {
    let updateActiveField ={
        isActive : true
    };

    return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then(product => true).catch(err => {
        console.log(err);
        return false;
    });
};


