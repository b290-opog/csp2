const Product = require("../models/Product");

// SINCE WE ARE NOT GONNA BE USING A DATABASE FOR CART, THIS ARRAY WILL ACT AS A PSEUDO ""DATABASE"". I HOPE IT WORKS WHEN I DEPLOY THIS. . . . .
const cart = [];


// ADD PRODUCT TO CART FUNCTION-INATOR - THIS WILL FIND IF THE USER'S CART IS ALREADY EXISTING IN THE ""DATABASE"", IF IT DOES ADD/MERGE PRODUCT IN THE CART, IF IT DOESN'T CREATE ONE.
module.exports.cartProduct = (userId, reqBody) => {
    return productData = Product.findById(reqBody.productId).then(product => {
        let subtotal = null;
        let quantity = null;
        let existingProductIndex = null;
        let existingUserIndex = null;
        for(let user of cart){
            if(userId == user.userId){
                existingUserIndex = (cart.indexOf(user));
                break;
            }
        };

        if(existingUserIndex !== null){
            for(let product of cart[existingUserIndex].products){
                if(product.productId == reqBody.productId){
                    existingProductIndex = (cart[existingUserIndex].products.indexOf(product));
                    break;
                }
            };

            if(existingProductIndex !== null){
                subtotal = (reqBody.quantity * product.price) + cart[existingUserIndex].products[existingProductIndex].subtotal;
                quantity = reqBody.quantity + cart[existingUserIndex].products[existingProductIndex].quantity;

                cart[existingUserIndex].products[existingProductIndex].subtotal = subtotal;
                cart[existingUserIndex].products[existingProductIndex].quantity = quantity;

                return true;
                
            }else{
                let addProductInUserCart = {
                    productId : product.id,
                    productName : product.name,
                    quantity : reqBody.quantity,
                    subtotal : (product.price * reqBody.quantity)
                };

                cart[existingUserIndex].products.push(addProductInUserCart);

                return true;
            }
        }else{
            let newCartUser = {
                userId : userId,
                products : [
                    {
                        productId : product.id,
                        productName : product.name,
                        quantity : reqBody.quantity,
                        subtotal : (product.price * reqBody.quantity)
                    }
                ]
            };

            cart.push(newCartUser);
            return true;
        }
    }).catch(err => {
        console.log(err);
        return false;
    });
};


// VIEW CART FUNCTION-INATOR!!!!!!!! - THIS RETURNS THE CART MATCHING THE USER'S ID
module.exports.viewCart = async (userId) => {
    let usersCart = null;

    for(let user of cart){
        if(userId == user.userId){
            usersCart = user;
            break;
        };
    };

    return usersCart;
};





// REMOVE FROM CART FUNCTION-INATOR - THIS CREATES A NEW ARRAY THEN TRANSFER ALL CART ITEMS ONE BY ONE, EXEPT FOR THE ONE BEING REQUESTED, FROM THE OLD CART ARRAY THEREFORE DELETING THE REQUESTED PRODUCT.
module.exports.removeItem = async (userId, reqBody) => {
    if(cart.length !== 0){
        let indexOfProductToRemove = null;
        for(let user of cart){
            if(userId == user.userId){
                if(user.products.length !== 0){
                    for(let product of user.products){
                        if(product.productId == reqBody.id){
                            indexOfProductToRemove = user.products.indexOf(product);
                            user.products.splice(indexOfProductToRemove, 1)
                            break;
                        };
                    };
                }else{
                    console.log({ cart : "user has no active cart" });
                    return false
                };
            };
        };
        return true;
    }else{
        return false;
    };
};

//SINGLE PRODUCT QUANTITY MODIFICATION FUNTION-INATOR - UPDATES THE PRODUCT QUANTITY AND SUBTOTAL IN CART
module.exports.changeQuantity = async (userId, reqBody) => {
    return Product.findById(reqBody.id).then(product => {
        let indexOfProductToUpdate = null;
        let productPrice = product.price;
        if(cart.length !== 0){
            for(let user of cart){
                if(userId == user.userId){
                    if(user.products.length !== 0){
                        for(let product of user.products){
                            if(product.productId == reqBody.id){
                                indexOfProductToUpdate = user.product.indexOf(product);

                                user.products[indexOfProductToUpdate].quantity = reqBody.quantity;
                                user.products[indexOfProductToUpdate].subtotal = (productPrice * reqBody.quantity);
                                indexOfProductToUpdate = null;
                                console.log(product);
                                break;
                            }else{
                                indexOfProductToUpdate = user.products.indexOf(product);
                                console.log(indexOfProductToUpdate);
                                // console.log(user.products);
                                console.log({ cart : "product not found in user's cart" });
                                return false;
                            };
                        };
                    }else{
                        console.log({ cart : "user has no products in cart" });
                        return false;
                    };
                    break;
                }else{
                    console.log({ cart : "user has no active cart" });
                    return false;
                };
            };
            
            return true;
        }else{
            console.log({ cart : "no user in cart" });
            return false;
        };
    }).catch(err => {
        console.log(err);
        return false
    });
};


//CART CHECKOUT FUNCTION - RETURNS ALL CARTED PRODUCTS AND THEIR TOTAL AMOUNT
module.exports.checkOut = async (userId) => {
    if(cart.length !== 0){
        for(let user of cart){
            if(user.userId == userId){
                if(user.products.length !== 0){
                    console.log(user);
                    let checkOutCart = {
                        userId : user.userId,
                        products : user.products,
                    };
                    let totalAmount = checkOutCart.products.reduce((total, item) => total + item.subtotal, 0);

                    checkOutCart.totalAmount = totalAmount;

                    return checkOutCart;
                }else{
                    console.log({ cart : "user has no products in cart" });
                    return false;
                };
            }else{
                console.log({ cart : "user has no active cart" });
                return false;
            };
        };
    }else{
        console.log({ cart : "no user in cart" });
        return false;
    };
};