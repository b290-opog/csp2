const express = require("express");
const router = express.Router();

const auth = require("../auth");
const cartController = require("../controllers/cartController");


// ADD SPECIFIC PRODUCT TO CART - IF PRODUCT ALREADY EXIST IN CART, IT WILL MERGE THE EXISTING AND UPCOMING PRODUCTS' QUANTITY AND SUBTOTAL
router.post("/add", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if(!userData.isAdmin){
        cartController.cartProduct(userData.id, req.body).then(resultFromController => res.send(resultFromController));
    }else{
        console.log({ auth : "unauthorized user" });
        res.send(false);
    };
});

// VIEW CART/CARTED PRODUCTS - SHOWS PRODUCT QUANTITY AND SUBTOTALS
router.get("/view", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if(!userData.isAdmin){
        cartController.viewCart(userData.id).then(resultFromController => res.send(resultFromController));
    }else{
        console.log({ auth : "unauthorized user" });
        res.send(false);
    };
});

// REMOVES SPECIFIC PRODUCT FROM CART
router.post("/remove", auth.verify,(req, res) => {
    const userData = auth.decode(req.headers.authorization);
    console.log(userData.id);
    if(!userData.isAdmin){
        cartController.removeItem(userData.id, req.body).then(resultFromController => res.send(resultFromController));
    }else{
        console.log({ auth : "unauthorized user" });
        res.send(false);
    };
});

// EDIT SPECIFIC PRODUCT QUANTITY
router.patch("/edit", auth.verify,(req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if(!userData.isAdmin){
        cartController.changeQuantity(userData.id, req.body).then(resultFromController => res.send(resultFromController));
    }else{
        console.log({ auth : "unauthorized user" });
        res.send(false);
    };
});


// CART CHECKOUT - SHOWS ALL CARTED PRODUCTS AND THEIR TOTAL
router.get("/checkOut", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if(!userData.isAdmin){
        cartController.checkOut(userData.id).then(resultFromController => res.send(resultFromController));
    }else{
        console.log({ auth : "unauthorized user" });
        res.send(false);
    };
});


module.exports = router;