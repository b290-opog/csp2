const express = require("express");
const router = express.Router();

const auth = require("../auth");
const userController = require("../controllers/userController");

// USER REGISTRATION ROUTE
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// USER LOGIN ROUTE
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// GET USER DETAILS ROUTE
router.get("/userDetails", auth.verify, (req, res) => {
    const userId = auth.decode(req.headers.authorization).id;
    userController.getUserDetails(userId).then(resultFromController => res.send(resultFromController));
});


//USER CHECKOUT (CREATE PRODUCT ORDERS)
router.post("/checkOut", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if(!userData.isAdmin){
        let data = {
            userId : userData.id,
            productId : req.body.productId,
            quantity : req.body.quantity
        };

        console.log("Data From Route:");
        console.log(data);
        userController.checkOut(data).then(resultFromController => res.send(resultFromController));
    };
});




/* STRETCH GOALS STARTS HERE */

// SET USER STATUS TO ADMIN !ADMIN ONLY!
router.patch("/:email/setAsAdmin", auth.verify, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    if(isAdmin){
        console.log(req.params);
        userController.setUserAdmin(req.params).then(resultFromController => res.send(resultFromController));
    }else{
        console.log({ auth : "unauthorized user" });
        res.send(false);
    };

});

// RETRIEVE AUTHENTICATED USER'S ORDERS
router.get("/myOrders", auth.verify, (req, res) => {
    const userId = auth.decode(req.headers.authorization).id;
    console.log(userId);
    userController.getUserOrders(userId).then(resultFromController => res.send(resultFromController));
});



// RETRIEVE ALL ORDERS !ADMIN ONLY!
router.get("/orders", auth.verify, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;
    if(isAdmin){
        userController.getAllOrdered().then(resultFromController => res.send(resultFromController))
    }else{
        console.log({ auth : "unauthorized user" });
        res.send(false);
    };
});









module.exports = router;