const express = require("express");
const router = express.Router();

const auth = require("../auth");
const productController = require("../controllers/productController");


// CREATE NEW PRODUCTS ROUTE
router.post("/", auth.verify, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;
    if(isAdmin){
        productController.addNewProduct(req.body).then(resultFromController => res.send(resultFromController));
    }else{
        console.log({ auth : "unauthorized user" });
        res.send(false);
    };
});

// GETTING ALL PRODUCTS ROUTE
router.get("/all", (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;
    if(isAdmin){
        productController.getAllProducts().then(resultFromController => res.send(resultFromController));
    }else{
        console.log({ auth : "unauthorized user" });
        res.send(false);
    };
});

// GETTING ALL ACTIVE PRODUCT ROUTE
router.get("/", (req, res) => {
    productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

// GETTING SINGLE PRODUCT ROUTE
router.get("/:productId", (req, res) => {
    console.log(req.params);
    productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// UPDATING PRODUCT ROUTE !ADMIN ONLY!
router.put("/:productId", auth.verify, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    if(isAdmin){
        console.log(req.params);
        productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
    }else{
        console.log({ auth : "unathorized user" });
        res.send(false)
    };
});


// ARCHIVING PRODUCTS ROUTE !ADMIN ONLY!
router.patch("/:productId/archive", auth.verify, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    if(isAdmin){
        console.log(req.params);
        productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
    }else{
        console.log({ auth : "unauthorized user" });
        res.send(false);
    };
});


// ACTIVATING PRODUCTS ROUTE !ADMIN ONLY!
router.patch("/:productId/activate", auth.verify, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    if(isAdmin){
        console.log(req.params);
        productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
    }else{
        console.log({ auth : "unauthorized user" });
        res.send(false);
    };
});









module.exports = router;