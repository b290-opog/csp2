const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

const productRoute = require("./routes/productRoute");
const userRoute = require("./routes/userRoute");
const cartRoute = require("./routes/cartRoute");


const app = express();

// MONGODB CONNECTION
mongoose.connect("mongodb+srv://admin:admin123@zuitt.mo2vnle.mongodb.net/eCommerce-API?retryWrites=true&w=majority",
    {
    useNewUrlParser : true,
    useUnifiedTopology : true
    }
);

mongoose.connection.once("open", () => console.log("Now connected to eCommerce-API DB"));

// MIDDLEWARES
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended : true }));

app.use("/products", productRoute);
app.use("/users", userRoute);
app.use("/cart", cartRoute);

if(require.main === module){
    app.listen(process.env.PORT || 4001, () => console.log(`API is now online on port ${process.env.PORT || 4001}`));
};

module.exports = { app, mongoose }